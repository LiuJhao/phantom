#ifndef IMPORTMODEL_H
#define IMPORTMODEL_H

#include "globals.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "TROOT.h"
#include "Control/Control.h"
#include <vector>

class ImportModel
{
  public:
    ImportModel();
    ~ImportModel();
    void DefineModel(G4LogicalVolume* world);
    G4LogicalVolume* GetModelLV();

  protected:
    void InitialisationOfMaterials();

  private:
    // data member of Model setting
    G4ThreeVector fModelSize;  //size of the entire Model
    G4int fNx, fNy, fNz;         //Number of segmentation of the Model
    G4LogicalVolume* fLVModelSens;
    G4LogicalVolume* logicModel;

    //double data_root[56][301][324];

protected:
    std::vector<G4Material*> fOriginalMaterals;
    std::vector<G4Material*> fMaterials;
    std::vector<int> fNumber;
};

#endif
