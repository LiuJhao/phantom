#include <iostream>
#include "TFile.h"
#include "TH2D.h"
#include "TString.h"
#include "TCanvas.h"
#include "TStyle.h"

using namespace std;

int load(void)
{
        ifstream loader("./lobster_301x324x56_uint8.raw",ios::in|ios::binary);

        if(loader.good()){
                cout<<"test file"<<endl;
                loader.seekg (0, loader.end);
                int length = loader.tellg();
                loader.seekg (0, loader.beg);
                cout<<"file length (bytes) #"<<length<<endl;
        }

        const size_t len = 301*324*56;
        cout<<"array length #"<<len<<endl;

        unsigned char* data = new unsigned char[len];
        for(int i=0;i<len;++i) data[i] = 0;

        loader.read(reinterpret_cast<char*>(data), len*sizeof(unsigned char));
        loader.close();
        TFile* ff = new TFile("lobster.root", "RECREATE");
        TH2D* himg[56];
        for(int d=0;d<56;++d){
                TString name=TString::Format("img%02d", d);
                TString title=TString::Format("slice %02d", d);
                himg[d] = new TH2D(name, title, 301,0.,301., 324,0.,324.);
        }



        for(int depth=0;depth<56;++depth){
                cout<<"process #"<<depth<<" images."<<endl;
                for(int h=0;h<324;++h){
                        for(int w=0;w<301;++w){
                                if((int)data[(depth*324+h)*301+w]>0)
                                  himg[depth]->SetBinContent(w,h, data[(depth*324+h)*301+w]);
                        }
                }
        }
        cout<<endl;

        TCanvas* plot = new TCanvas("lobster","slice", 400,400);
        gStyle->SetOptStat(0);
        plot->Print("lobster.pdf[");

        for(int d=0;d<56;++d){
                himg[d]->Write("", TObject::kOverwrite);
                himg[d]->Draw("colz");
                plot->Print("lobster.pdf");
        }
        plot->Print("lobster.pdf]");

        delete[] data;

        return 0;
}

