#include "G4NistManager.hh"
#include "G4GDMLParser.hh"
#include "G4SolidStore.hh"
#include "G4UserLimits.hh"
#include "G4GeometryManager.hh"
#include "G4GeometryTolerance.hh"
#include "G4PVParameterised.hh"
#include "G4VisAttributes.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4SDManager.hh"
#include "G4LogicalVolume.hh"

#include "iostream"
#include <iterator>
#include <filesystem>
#include <vector>
///////////////////////////////////////////

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TH2D.h"
#include "TString.h"
#include "TCanvas.h"
#include "TStyle.h"

#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4Box.hh"

#include "Geometry/NestedParameterisation.h"
#include "Geometry/ImportModel.h"
#include "Control/Control.h"

using namespace std;

ImportModel::ImportModel()
{
    fModelSize.setX(pControl->RawModelSize.x());
    fModelSize.setY(pControl->RawModelSize.y());
    fModelSize.setZ(pControl->RawModelSize.z());
    fNx = pControl->RawVoxel.x();
    fNy = pControl->RawVoxel.y();
    fNz = pControl->RawVoxel.z();

    cout<<"voxel number in x directory: "<<fNx<<endl;
    cout<<"voxel number in y directory: "<<fNy<<endl;
    cout<<"voxel number in z directory: "<<fNz<<endl;
}

ImportModel::~ImportModel()
{}

//Initialisation of the original materials
void ImportModel::InitialisationOfMaterials()
{
    // Creating elements :
    G4double z, a, density;
    G4String name, symbol;

    G4Element* elC = new G4Element( name = "Carbon",
                                   symbol = "C",
                                   z = 6.0, a = 12.011 * g/mole );
    G4Element* elH = new G4Element( name = "Hydrogen",
                                   symbol = "H",
                                   z = 1.0, a = 1.008  * g/mole );
    G4Element* elN = new G4Element( name = "Nitrogen",
                                   symbol = "N",
                                   z = 7.0, a = 14.007 * g/mole );
    G4Element* elO = new G4Element( name = "Oxygen",
                                   symbol = "O",
                                   z = 8.0, a = 16.00  * g/mole );
    G4Element* elNa = new G4Element( name = "Sodium",
                                    symbol = "Na",
                                    z= 11.0, a = 22.98977* g/mole );
    G4Element* elMg = new G4Element( name = "Magnesium",
                                    symbol = "Mg",
                                    z = 12.0, a = 24.3050* g/mole );
    G4Element* elP = new G4Element( name = "Phosphorus",
                                   symbol = "P",
                                   z = 15.0, a = 30.973976* g/mole );
    G4Element* elS = new G4Element( name = "Sulfur",
                                   symbol = "S",
                                   z = 16.0,a = 32.065* g/mole );
    G4Element* elCl = new G4Element( name = "Chlorine",
                                    symbol = "P",
                                    z = 17.0, a = 35.453* g/mole );
    G4Element* elK = new G4Element( name = "Potassium",
                                   symbol = "P",
                                   z = 19.0, a = 30.0983* g/mole );

    G4Element* elFe = new G4Element( name = "Iron",
                                    symbol = "Fe",
                                    z = 26, a = 56.845* g/mole );
 
    G4Element* elCa = new G4Element( name="Calcium",
                                    symbol = "Ca",
                                    z = 20.0, a = 40.078* g/mole );

    G4Element* elZn = new G4Element( name = "Zinc",
                                   symbol = "Zn",
                                   z = 30.0,a = 65.382* g/mole );

    // Creating Materials :
    G4int numberofElements;

    // Soft tissue (ICRP - NIST)
    G4Material* softTissue = new G4Material ("SoftTissue", 5.00*g/cm3, 
                                             numberofElements = 13);
    softTissue->AddElement(elH, 10.4472*perCent);
    softTissue->AddElement(elC, 23.219*perCent);
    softTissue->AddElement(elN, 2.488*perCent);
    softTissue->AddElement(elO, 63.0238*perCent);
    softTissue->AddElement(elNa, 0.113*perCent);
    softTissue->AddElement(elMg, 0.0113*perCent);
    softTissue->AddElement(elP, 0.113*perCent);
    softTissue->AddElement(elS, 0.199*perCent);
    softTissue->AddElement(elCl, 0.134*perCent);
    softTissue->AddElement(elK, 0.199*perCent);
    softTissue->AddElement(elCa, 0.023*perCent);
    softTissue->AddElement(elFe, 0.005*perCent);
    softTissue->AddElement(elZn, 0.003*perCent);

    // Water
    G4Material* water = new G4Material( "Water",
                                       density = 1.0*g/cm3,
                                       numberofElements = 2 );
    water->AddElement(elH,0.112);
    water->AddElement(elO,0.888);

    // store the original materals 
    fOriginalMaterals.push_back(softTissue); //1.055 g/cm3
    fOriginalMaterals.push_back(water); //1.018 g/cm3
}


void ImportModel::DefineModel(G4LogicalVolume* world)
{
    ///////////////////////////////
    //    Read root data file    //
    ///////////////////////////////

    G4String root_file;
    root_file = pControl->raw_model_directory+"/"+ pControl->raw_model_name+".root";
    TFile *file = new TFile(root_file);

    ///////////////////////////////////////
    //    Initialize vector data_root    //
    ///////////////////////////////////////
    vector<vector<vector<int>>> data_root(fNx,vector<vector<int>>(fNy, vector<int>(fNz, 0)));

    for (int z=0; z<fNz;z++)
    {
        char hist[100];
        if (z < 10) {sprintf(hist, "img0%d", z);}
        else {sprintf(hist, "img%d", z);}

        TH2D *slice = nullptr;
        slice = (TH2D*)file->Get(hist);
        for(int x=1; x<=fNx; x++)
        {
            for(int y=1; y<=fNy; y++)
            {
                Int_t bin=slice->GetBin(x,y,0);
                double bincontent=slice->GetBinContent(bin);
                data_root[x-1][y-1][z] = bincontent;
            }
        }
        delete slice;
    }

    ////////////////////////////////////
    //    Materials initialisation    //
    ////////////////////////////////////
    InitialisationOfMaterials();

    G4NistManager* NISTman = G4NistManager::Instance();
    G4Material* water  = NISTman->FindOrBuildMaterial("G4_WATER");

    // materials map
    
    for(int z=0; z<fNz; z++)
    {
        for(int y=0; y<fNy; y++)
        {
            for(int x=0; x<fNx; x++)
            {
                if(data_root[x][y][z]>=50)
                {
                    fMaterials.push_back(fOriginalMaterals.at(0));
                    fNumber.push_back(0);
                }
                else
                {
                    fMaterials.push_back(fOriginalMaterals.at(1));
                    fNumber.push_back(1);
                }
            }
        }
    }
    
    ///////////////////////////////
    //    Entire Model size    //
    ///////////////////////////////
    
    //world volume have been define above which we can
    // use directly

    //..............................................
    // mother volume of the target Model
    //..............................................

    G4ThreeVector Modelsize = fModelSize;

    G4Box* solidModel 
      = new G4Box("Model", Modelsize.x()/2., Modelsize.y()/2., Modelsize.z()/2.);

    logicModel = new G4LogicalVolume(solidModel, water, "Model", 0,0,0);

    G4ThreeVector positionModel;
    new G4PVPlacement(0,                   // no rotation
                      positionModel,     // position
                      logicModel,        // its logical volume
                      "Model",           // name
                      world,            // its mother volume
                      false,               // no boolean operations
                      0);                  // copy number
  
    //..............................................
    // segmentation using parameterisation
    //..............................................
    
    //number of segmentation
    G4int nxcells = fNx;
    G4int nycells = fNy;
    G4int nzcells = fNz;

    G4ThreeVector sensSize;
    sensSize.setX(Modelsize.x()/(G4double)nxcells);
    sensSize.setY(Modelsize.y()/(G4double)nycells);
    sensSize.setZ(Modelsize.z()/(G4double)nzcells);
    // Voxel size will be 1.0 x 1.0 x 1.0 mm3 cube by default.

    // Replication of Model volume
    // Y slice
    G4String yRepname("RepY");
    G4VSolid* solYRep = 
      new G4Box(yRepname, Modelsize.x()/2., sensSize.y()/2., Modelsize.z()/2.);
    G4LogicalVolume* logYRep=
      new G4LogicalVolume(solYRep, water, yRepname);

    new G4PVReplica(yRepname, logYRep, logicModel, kYAxis, fNy, sensSize.y());

    logYRep->SetVisAttributes(new G4VisAttributes(G4VisAttributes::GetInvisible()));

    // X slice
    G4String xRepname("RepX");
    G4VSolid* solXRep = 
      new G4Box(xRepname, sensSize.x()/2., sensSize.y()/2., Modelsize.z()/2.);
    G4LogicalVolume* logXRep = 
      new G4LogicalVolume(solXRep, water, xRepname);

    new G4PVReplica(xRepname, logXRep, logYRep, kXAxis, fNx, sensSize.x());
    
    logXRep->SetVisAttributes(new G4VisAttributes(G4VisAttributes::GetInvisible()));

    // Z slice
    G4String zVoxName("ModelSens");
    G4VSolid* solVoxel = 
      new G4Box(zVoxName, sensSize.x()/2., sensSize.y()/2., sensSize.z()/2.);

    fLVModelSens = new G4LogicalVolume(solVoxel, water, zVoxName);

    NestedParameterisation* paramModel = 
      new NestedParameterisation(sensSize/2., fNx, fNy, fNz, fOriginalMaterals, fNumber);  

    new G4PVParameterised("ModelSens",
                          fLVModelSens,
                          logXRep,
                          kUndefined,
                          nzcells,
                          paramModel);  

}

G4LogicalVolume* ImportModel::GetModelLV()
{
    return logicModel;
}