#include "Geometry/NestedParameterisation.h"

#include "G4VPhysicalVolume.hh"
#include "G4VTouchable.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4VisAttributes.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
NestedParameterisation
::NestedParameterisation(const G4ThreeVector& voxelsize,
                         G4int fnX_, G4int fnY_, G4int fnZ_, 
                         std::vector<G4Material*> mat, std::vector<int> num)
:G4VNestedParameterisation(),
  fdX(voxelsize.x()),fdY(voxelsize.y()),fdZ(voxelsize.z()),
  fnX(fnX_),fnY(fnY_),fnZ(fnZ_),
  fMat(mat), fnum(num),
  count(0)
{
  // Position of voxels. 
  // x and y positions are already defined in DetectorConstruction 
  // by using replicated volume. Here only we need to define is z positions
  // of voxles.

    fpZ.clear();
    G4double zp;
    for( G4int iz = 0; iz < fnZ; iz++)
    {
        zp = (-fnZ+1+2*iz)*fdZ;
        fpZ.push_back(zp); // total 100 elements
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
NestedParameterisation::~NestedParameterisation()
{
    fpZ.clear();
}


G4Material* NestedParameterisation::ComputeMaterial(G4VPhysicalVolume* physVol, const G4int iz,
                                                    const G4VTouchable* parentTouch)
{
    if(parentTouch==nullptr)
        return fMat[0];  // index 0 point to the water
    
    // Copy number of voxels
    // Copy number of X and Y are obtain from replication number
    // Copy number of Z is the copy number of current voxel.

    G4int ix = parentTouch->GetReplicaNumber(0);
    G4int iy = parentTouch->GetReplicaNumber(1);

    G4int copyID = ix + fnX*iy + fnX*fnY*iz;

    // std::cout<<"copy id :"<<copyID<<std::endl;
    // count += 1;
    // std::cout<<"counting: "<<count<<std::endl;
    // std::cout<<"current ix is: "<<ix<<std::endl;
    // std::cout<<"current iy is: "<<iy<<std::endl;
    // std::cout<<"current iz is: "<<iz<<std::endl;

    G4int number = fnum.at(copyID);
    
    static G4Material* mate = nullptr;
    if (number == 0)
    {
        mate = fMat.at(0);
    }
    else if (number == 1)
    {
        mate = fMat.at(1);
    }
    

    G4Color color1(1.0,0.0,0.0);
    G4Color color2(0.0,0.0,0.0);
    G4Color color3(0.0,0.0,1.0);
    G4String mateName = fMat.at(number)->GetName();
    if(mateName == "SoftTissue")
    {
        auto color_tar = new G4VisAttributes(color1);
        color_tar->SetVisibility(true);
        color_tar->SetForceSolid();
        physVol->GetLogicalVolume()->SetVisAttributes(color_tar);
    }
    else
    {
        auto color_tar = new G4VisAttributes(color2);
        color_tar->SetVisibility(false);
        //color_tar->SetForceSolid();
        physVol->GetLogicalVolume()->SetVisAttributes(color_tar);
    }

    return mate;
}                 

G4int NestedParameterisation::GetNumberOfMaterials() const
{
    return fMat.size();
}

G4Material* NestedParameterisation::GetMaterial(G4int i) const
{
    return fMat[i];
}

void NestedParameterisation::ComputeTransformation(const G4int no,
                                                   G4VPhysicalVolume *currentPV) const
{
    G4ThreeVector position(0.,0., fpZ[no]);
    currentPV->SetTranslation(position);
}

void NestedParameterisation::ComputeDimensions(G4Box& box, const G4int, const G4VPhysicalVolume* ) const
{
    box.SetXHalfLength(fdX);
    box.SetYHalfLength(fdY);
    box.SetZHalfLength(fdZ);
}